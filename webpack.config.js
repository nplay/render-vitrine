const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
    mode: 'development',
    output: {
      filename: 'bundle.js',
      path: `/enviroment/www/cdn/vitrine/1.0.0/render/js/`,
      //libraryTarget: 'umd',
      //library: 'Vitrine'
    },
    resolve: {
        alias: {
          'vue$': 'vue/dist/vue.esm.js' // 'vue/dist/vue.common.js' for webpack 1
        }
    },
    module:{
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
                },
                // this will apply to both plain `.js` files
                // AND `<script>` blocks in `.vue` files
                {
                test: /\.js$/,
                loader: 'babel-loader'
                },
                // this will apply to both plain `.css` files
                // AND `<style>` blocks in `.vue` files
                {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'vue-style-loader',
                    'css-loader',
                ]
                }
        ]
    },
    plugins: [
      // make sure to include the plugin!
      new VueLoaderPlugin()
    ]
};
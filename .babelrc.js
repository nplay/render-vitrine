const presets = [
  [
    "@babel/preset-env",
    {
      "targets": {
        "node": "10"
      }
    }
  ]
];
const plugins = [];

module.exports = { presets, plugins };

import SwiperPlugin from './plugins/swiper/swiperPlugin'
import AnalyticsPlugin from './plugins/analytics/analyticsPlugin'

import RenderEngine from 'render-engine'

class Render
{
    constructor(selector, html, data)
    {
       this.selector = selector
       this.html = html
       
       this.data = data
       this.data.uid = this.id

       this.component = null

       this.swiperPlugin = new SwiperPlugin()
       this.analyticsPlugin = new AnalyticsPlugin()
    }

    get id()
    {
        let dId = JSON.parse(window.atob(token.split('.')[1])).dId
        return dId + '-' + this.data.theme._id + '-' + this.data.line._id
    }

    async mountVueComponent()
    {
        let renderEngine = new RenderEngine(this.html, this.data)

        let component = await renderEngine.component()

        component.events.addEventListener('mountBefore', (evt) => {
            setTimeout(() => {
                this.analyticsPlugin.mountAfter(this.data)
            }, 100)
        })

        component.events.addEventListener('mountAfter', (evt) => {
            setTimeout(() => {
                this._runSwiper(evt)

                let element = evt.detail.target.$el.querySelector(`.swiper-container`)
                this.analyticsPlugin.mountBefore(Object.assign({element: element}, this.data)).run()
            }, 100)
        })
        
        component.mount()

        let container = this.querySelector(this.selector)
        container.appendChild(component.get.$el)
    }

    _runSwiper(evt)
    {
        let element = evt.detail.target.$el.querySelector(`.swiper-container`)
        this.swiperPlugin.mount(element, this.data.swiper).run()
    }

    querySelector(elementSelector)
    {
        if(typeof jQuery != 'function')
            throw new Error('Jquery library not initialized')
        
        let result = jQuery(elementSelector)
        if(result.length == 0)
           throw new Error(`Element not found by xpath: ${elementSelector}`)
        
        return result[0]
    }
}

export default Render

class AnalyticsPlugin
{
    constructor()
    {
        this._objAnalytics = null

        this._nodes = new Array
        this._itens = new Array

        this._line = null
        this._theme = null

        this._slides = new Array
    }

    mountAfter(data)
    {
        this._theme = data.theme
        this._line = data.line

        this._itens = data.itens

        return this
    }

    mountBefore(data)
    {
        let container = data.element
        this._slides = container.querySelectorAll('.swiper-slide')

        return this
    }

    run()
    {
        let analytics = window.vitrineApp.clients.get(token).publicLibraries.analytics

        for(let idx = 0; idx < this._slides.length; ++idx)
        {
            let node = this._slides[idx]
            let item = this._itens[idx]
            
            node.addEventListener('click', () => {
                let data = [
                    {
                        originCollection: [
                            {
                                title: this._line.title,
                                id: this._line._id,
                                node: {
                                    id: "lineCollection",
                                    name: "Vitrine"
                                }
                            },
                            {
                                title: this._theme.name,
                                id: this._theme._id,
                                node: {
                                    id: "theme",
                                    name: "Tema"
                                }
                            }
                        ],
                        product: item
                    }
                ]

                analytics.track.prospectItem(data)
            })
        }
    }
}

export default AnalyticsPlugin

var SwiperLibrary = require('swiper');
import Resize from './on/resize'

/**
 * MANAGE AND INITIALIZE SWIPER
 */
class SwiperPlugin
{
    constructor()
    {
        this.data = {};
        this.container = null;
        this._objSwiper = null;

        var resize = new Resize()

        this.configuration = new Object({
            slidesPerView: null,
            slidesPerColumn: null,
            slidesPerColumnFill: null,
            slidesPerGroup: null,
            simulateTouch: true,
            spaceBetween: 0,
            on: {
                resize: resize.initialize.bind(resize, this),
                init: resize.initialize.bind(resize, this)
            },
            breakpoints: null,
            navigation: null,
            loop: null
        })
    }

    setData(data)
    {
        this.data = data
    }

    mount(container, parameters)
    {
        this.data = Object.assign(parameters, this.data)
        
        this._initializeConfiguration()
        this.container = container
        
        return this
    }

    run()
    {
        this._objSwiper = new SwiperLibrary.default(this.container, this.configuration);
        window.swiper = this._objSwiper
    }

    _initializeConfiguration()
    {
        for(let key of Object.keys(this.data))
        {
            if(this.configuration.hasOwnProperty(key) == false)
                continue;

            this.configuration[key] = this.data[key]
        }

        for(let key of Object.keys(this.configuration))
        {
            if(this.configuration[key] == null)
                delete this.configuration[key]
        }
    }
}

export default SwiperPlugin

import Render from "./render"

class RenderCollection
{
    constructor()
    {
        this.collection = new Array
    }

    get(idx)
    {
        return this.collection.find(item => {
            return item.elementRef == idx
        })
    }

    append(selector, themeModel)
    {
        let HTML = themeModel.designModel.html

        themeModel.swiper.slidesPerView = themeModel.swiper.slidesPerView == 0 ? 'auto' : themeModel.swiper.slidesPerView
        let swiperData = themeModel.swiper

        for(let line of themeModel.themeLineCollection)
        {
            let render = new Render(selector, HTML, {
                itens: line.componentModel.itens,
                swiper: Object.assign(line.swiper, swiperData),
                line: line,
                theme: themeModel
            })

            this.collection.push(render)
        }
    }
}

export default RenderCollection

import axios from 'axios'
import CacheAPI from './cacheAPI'
import Parameter from './abstractAPI/parameter'

class AbstractAPI
{
    constructor(customData, cache)
    {
        this.URL_ROOT = window.vitrineApp.API.URL
        this.URL_PATH = null
        this._customData = new Object(customData)
        this._parameters = new Object
        this.useCache = cache == undefined ? false : cache

        this.parameters = new Parameter
    }

    async post(url, body)
    {
        return await this._request(url, body, 'POST')
    }

    async get(url)
    {
        return await this._request(url, {}, 'get')
    }

    async buildParameters()
    {
    }

    _setUrl()
    {
        throw new Error(`Please implement in child class`)
    }

    _getCacheKey(url, requestData)
    {
        let obj = Object.assign({url: url}, requestData)
        let base64 = window.btoa(JSON.stringify(obj))
        return base64
    }

    async _request(url, body, method)
    {
        await this.buildParameters()

        let requestData = {
            method: method,
            headers: {'Authorization-token': token},
            responseType: 'json',
            validateStatus: function (status) {
                return status == 200 || status == 400; // default
            },
            params: this.parameters.raw,
            data: body
        }
        
        let cacheAPI = new CacheAPI(this._getCacheKey(url, requestData))
        let data = !this.useCache ? null : cacheAPI.getCache()

        if(data != null)
            return JSON.parse(data);

        let response = await axios.request(url, requestData);

        if(this.useCache)
            cacheAPI.setCache(JSON.stringify(response))
        
        return response
    }
}

export default AbstractAPI;

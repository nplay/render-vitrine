import AbstractAPI from "../abstractAPI";

class AbstractSearchesAPI extends AbstractAPI
{
    constructor(customData, cache)
    {
        super(customData, cache)

        this.client = window.vitrineApp.clients.get(token)
    }

    async buildParameters()
    {
        this.parameters.limit = this._customData.theme.maxItens
    }
}

export default AbstractSearchesAPI
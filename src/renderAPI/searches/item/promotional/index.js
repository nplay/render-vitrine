
import AbstractSearchesAPI from "../../abstractSearchesAPI";

class PromotionalIndexAPI extends AbstractSearchesAPI
{
    constructor(customData)
    {
        super(customData, true);

        this.URL_PATH = "searches/item/promotional/";
        this.URL = this._setUrl()
    }

    _setUrl()
    {
        let url = `${this.URL_ROOT}${this.URL_PATH}`
        return url;
    }

    async get()
    {
        return await super.get(this.URL, {})
    }

    async getData()
    {
        let response = await this.get();
        return response.data.data
    }
}

export default PromotionalIndexAPI;
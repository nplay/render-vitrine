
import AbstractSearchesAPI from "../../abstractSearchesAPI";

class PromotionalItemCategoryAPI extends AbstractSearchesAPI {
   constructor(customData) {
      super(customData, true);

      this.URL_PATH = "searches/item/promotional/attribute/";
      this.URL = this._setUrl()
   }

   _setUrl() {
      let url = `${this.URL_ROOT}${this.URL_PATH}`
      return url
   }

   async get() {
      let curItem = this.client.publicLibraries.analytics.pageInformation.item() || null
      let curItens = this.client.publicLibraries.analytics.pageInformation.itens() || new Array

      if(curItem)
         curItens.push(curItem)
      
      let itens = curItens.map(item => { return item.id })

      let body = {
         "ids": itens,

         "attributes": [
            "categorys"
         ]
      }

      return await super.post(this.URL, body)
   }

   async getData() {
      let response = await this.get();
      return response.data.data
   }
}

export default PromotionalItemCategoryAPI;

import AbstractSearchesAPI from "../../abstractSearchesAPI";

class ViewToViewAPI extends AbstractSearchesAPI
{
    constructor(customData)
    {
        super(customData, true);

        this.URL_PATH = "searches/item/view-to-view/";
        this.URL = this._setUrl()
    }

    _setUrl()
    {
        let url = `${this.URL_ROOT}${this.URL_PATH}`

        return url
    }

    async get()
    {
        //Retrive Analytics current item of Page
        let curItem = this.client.publicLibraries.analytics.pageInformation.item() || null
        let curItens = this.client.publicLibraries.analytics.pageInformation.itens() || new Array
  
        if(curItem)
           curItens.push(curItem)
        
        return await super.post(this.URL, {
            "ecommerce": {
                "impressions": curItens
            }
        })
    }

    async getData()
    {
        let response = await this.get();
        return response.data.data
    }
}

export default ViewToViewAPI;
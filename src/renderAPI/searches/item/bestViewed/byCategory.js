
import AbstractSearchesAPI from "../../abstractSearchesAPI";

class IndexAPI extends AbstractSearchesAPI
{
    constructor(customData)
    {
        super(customData, true);

        this.URL_PATH = "searches/item/best-viewed/";
        this.URL = this._setUrl()
    }

    _setUrl()
    {
        let url = `${this.URL_ROOT}${this.URL_PATH}`

        if(!this._customData.line.additionalData.hasOwnProperty('categoryID'))
            throw new Error(`Category not found`)

        url += `category/${this._customData.line.additionalData.categoryID}`

        return url
    }

    async get()
    {
        return await super.get(this.URL, {})
    }

    async getData()
    {
        let response = await this.get();
        return response.data.data
    }
}

export default IndexAPI;
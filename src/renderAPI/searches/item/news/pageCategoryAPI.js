
import AbstractSearchesAPI from "../../abstractSearchesAPI";

class NewsPageCategoryAPI extends AbstractSearchesAPI
{
    constructor(customData)
    {
        super(customData, true);

        this.URL_PATH = "searches/item/new/";
        this.URL = this._setUrl()
    }

    _setUrl()
    {
        let analytics = this.client.publicLibraries.analytics
        let categoryID = analytics.pageInformation.category()
    
        if(categoryID == null)
            throw new Error('Category not found in datalayer')

        let url = `${this.URL_ROOT}${this.URL_PATH}`
        url += `category/${categoryID}`

        return url
    }

    async get()
    {
        return await super.get(this.URL, {})
    }

    async getData()
    {
        let response = await this.get();
        return response.data.data
    }
}

export default NewsPageCategoryAPI;
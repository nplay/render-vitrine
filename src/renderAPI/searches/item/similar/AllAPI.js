
import AbstractSearchesAPI from "../../abstractSearchesAPI";

class SimilarAPI extends AbstractSearchesAPI
{
    constructor(customData)
    {
        super(customData, true);

        this.URL_PATH = "searches/item/similar/";
        this.URL = this._setUrl()
    }

    _setUrl()
    {
        let url = `${this.URL_ROOT}${this.URL_PATH}`

        return url
    }

    async get()
    {
        //Retrive Analytics current item(s) of Page
        let curItem = this.client.publicLibraries.analytics.pageInformation.item() || null
        let curItens = this.client.publicLibraries.analytics.pageInformation.itens() || new Array
  
        if(curItem)
           curItens.push(curItem)
        
        let itens = curItens.map(item => { return item.id })

        return await super.post(this.URL, {
            "ecommerce": {
                "impressions": itens
            }
        })
    }

    async getData()
    {
        let response = await this.get();
        return response.data.data
    }
}

export default SimilarAPI;
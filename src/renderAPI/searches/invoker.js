
import BestSellerItemAPI from './item/bestSeller/allAPI'
import BestSellerItemByCategoryAPI from './item/bestSeller/byCategory'
import ViewedIndexAPI from './item/bestViewed/index'
import ViewedByCategoryIndexAPI from './item/bestViewed/byCategory'
import ViewedPageCategoryAPI from './item/bestViewed/pageCategoryAPI';
import NewIndexApi from './item/news/index'
import NewIndexByCategoryAPI from './item/news/byCategory'
import PromotionalIndexAPI from './item/promotional/index'
import PromotionalByCategoryAPI from './item/promotional/byCategory'
import BestSellerPageCategoryAPI from './item/bestSeller/pageCategoryAPI';
import NewsPageCategoryAPI from './item/news/pageCategoryAPI';
import HistoryItemAPI from './history/index'
import HistoryItemByCategoryAPI from './history/byCategory'
import HistoryPageCategoryAPI from './history/pageCategoryAPI'
import ViewToViewAPI from './item/viewToView/AllAPI'
import SimilarAPI from './item/similar/AllAPI';
import PromotionalPageCategoryAPI from './item/promotional/pageCategoryAPI'
import NewsItemCategoryAPI from './item/news/itemCategoryAPI'
import BestSellerItemCategoryAPI from './item/bestSeller/itemCategoryAPI';
import ViewedItemCategoryAPI from './item/bestViewed/itemCategoryAPI';
import PromotionalItemCategoryAPI from './item/promotional/itemCategoryAPI'
import HistoryItemCategoryAPI from './history/itemCategoryAPI'
import BuyToBuyAPI from './item/buyToBuy/AllAPI';

class Invoker
{
    constructor(searchType, customData)
    {
        this.searchType = searchType
        this.customData = customData
    }

    _getAvailableSearchs()
    {
        return {
            1: BestSellerItemAPI,
            2: ViewedIndexAPI,
            3: NewIndexApi,
            4: PromotionalIndexAPI,
            5: BestSellerPageCategoryAPI,
            6: NewsPageCategoryAPI,
            7: ViewedPageCategoryAPI,
            8: HistoryItemAPI,
            9: HistoryPageCategoryAPI,
            10: ViewToViewAPI,
            11: SimilarAPI,
            12: PromotionalPageCategoryAPI,
            13: NewsItemCategoryAPI,
            14: BestSellerItemCategoryAPI,
            15: ViewedItemCategoryAPI,
            16: PromotionalItemCategoryAPI,
            17: HistoryItemCategoryAPI,
            18: BuyToBuyAPI,

            19: BestSellerItemByCategoryAPI,
            20: ViewedByCategoryIndexAPI,
            21: HistoryItemByCategoryAPI,
            22: NewIndexByCategoryAPI,
            23: PromotionalByCategoryAPI
        }
    }

    get()
    {
        let availableSearchs = new Object(this._getAvailableSearchs())
        
        if(!availableSearchs.hasOwnProperty(this.searchType))
            throw new Error(`Search type '${this.searchType}' not implemented render`)

        let classType = availableSearchs[this.searchType]
        return new classType(this.customData)
    }
}

export default Invoker
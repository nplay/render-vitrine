import AbstractSearchesAPI from "../abstractSearchesAPI";

class HistoryPageCategoryAPI extends AbstractSearchesAPI
{
    constructor(customData)
    {
        super(customData, false);

        this.URL_PATH = "searches/history/item/";
        this.URL = this._setUrl()
    }

    _setUrl()
    {
        let analytics = this.client.publicLibraries.analytics
        let categoryID = analytics.pageInformation.category()
    
        if(categoryID == null)
            throw new Error('Category not found in datalayer')

        let url = `${this.URL_ROOT}${this.URL_PATH}`
        url += `category/${categoryID}`

        return url
    }

    async get()
    {
        return await super.post(this.URL, {
            sessionID: await this.client.user.sessionID
        })
    }

    async getData()
    {
        let response = await this.get();
        let itens = response.data.data

        return itens
    }
}

export default HistoryPageCategoryAPI;
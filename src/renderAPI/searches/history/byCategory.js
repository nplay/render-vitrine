import AbstractSearchesAPI from "../abstractSearchesAPI";

class HistoryItemAPI extends AbstractSearchesAPI
{
    constructor(customData)
    {
        super(customData, false);

        this.URL_PATH = "searches/history/item/";
        this.URL = this._setUrl()
    }

    _setUrl()
    {
        let url = `${this.URL_ROOT}${this.URL_PATH}`

        if(!this._customData.line.additionalData.hasOwnProperty('categoryID'))
            throw new Error(`Category not found`)

        url += `category/${this._customData.line.additionalData.categoryID}`

        return url
    }

    async get()
    {
        return await super.post(this.URL, {
            sessionID: await this.client.user.sessionID
        })
    }

    async getData()
    {
        let response = await this.get();

        if(!response.data.success)
            throw new Error(response.data.data)
        
        let itens = response.data.data

        return itens
    }
}

export default HistoryItemAPI;
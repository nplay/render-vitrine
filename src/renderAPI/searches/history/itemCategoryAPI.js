
import AbstractSearchesAPI from "../abstractSearchesAPI";

class HistoryItemCategoryAPI extends AbstractSearchesAPI
{
    constructor(customData)
    {
        super(customData, false);

        this.URL_PATH = "searches/history/item/attribute/";
        this.URL = this._setUrl()
    }

    _setUrl()
    {
        let url = `${this.URL_ROOT}${this.URL_PATH}`
        return url
    }

    async get()
    {
        let curItem = this.client.publicLibraries.analytics.pageInformation.item() || null
        let curItens = this.client.publicLibraries.analytics.pageInformation.itens() || new Array
  
        if(curItem)
           curItens.push(curItem)
        
        let itens = curItens.map(item => { return item.id })

        let body = {
            "sessionID": await this.client.user.sessionID,
            "ids": itens,
            
            "attributes": [
                "categorys"
            ]
        }
  
        return await super.post(this.URL, body)
    }

    async getData()
    {
        let response = await this.get();
        let itens = response.data.data

        return itens
    }
}

export default HistoryItemCategoryAPI;
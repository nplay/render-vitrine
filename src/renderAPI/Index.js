
import ThemeAPI from './themeAPI'
import Invoker from './searches/invoker'

class Index
{
    constructor()
    {
        this._themeAPI = new ThemeAPI();
    }

    async getThemeCollection()
    {
        if(typeof jQuery != 'function')
            throw new Error('Library jQuery not initialized')

        let response = await this._themeAPI.getSite()
        let themeCollection = response.data.data

        themeCollection = themeCollection.filter(theme => {
            //Detect exists xpath
            let exists = jQuery(theme.xpath).length > 0

            if(!exists)
                return exists
            
            //Filter themeLine enabled
            theme.themeLineCollection = theme.themeLineCollection.filter(line => {
                return line.enabled
            })

            return theme.themeLineCollection.length > 0
        })

        for(let theme of themeCollection)
        {
            try
            {
                await this._setItem(theme)
            } catch (error) {
                console.error('Fail in load theme', error.toString())
            }
        }

        return themeCollection
    }

    async _setItem(theme)
    {
        let linesDelete = new Array

        for(let themeLine of theme.themeLineCollection)
        {
            try {
                let component = themeLine.componentModel
                let typeSearch = component.searchTypeId
                
                let data = {
                    theme: theme,
                    line: themeLine
                }

                let API = new Invoker(typeSearch, data).get();
                let itens = await API.getData()

                itens = itens || new Array
                
                if(itens.length > 0)
                    component.itens = itens
                else
                    linesDelete.push(themeLine)
            } catch (error) {
                linesDelete.push(themeLine)
                console.error('Fail in load theme line', error.toString())
            }
        }

        linesDelete.forEach((line) => {
            let idx = theme.themeLineCollection.indexOf(line)
            theme.themeLineCollection.splice(idx, 1)
        })
    }
}

export default Index;
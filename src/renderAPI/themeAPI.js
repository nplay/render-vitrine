
import AbstractAPI from "./abstractAPI"

class ThemeAPI extends AbstractAPI
{
    constructor()
    {
        super(null, true);

        this.URL_PATH = "themes/site/"
        this._setUrl()
    }

    _setUrl()
    {
        this.URL = this.URL_ROOT+this.URL_PATH
    }

    async getSite()
    {
        let currentPage = window.vitrineApp.clients.get(token).publicLibraries.analytics.pageInformation.page()

        return await this.post(this.URL, {
            "filters": {
                "enabled": true,
                "additionalData.typeTheme": currentPage
            },
            "attributes": {}
        })
    }
}

export default ThemeAPI
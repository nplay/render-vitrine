
/**
 * DISPONIBILIZE RENDER COLLECTION IN BROWSER
 */
import RenderCollection from "./RenderCollection"
import Index from "../src/renderAPI/Index"

class RenderLibrary
{
  constructor()
  {
    this.renderCollection = new RenderCollection()
  }

  async _getThemes()
  {
    let renderAPI = new Index()
    let themes = await renderAPI.getThemeCollection()
    
    return themes
  }

  async getCollection()
  {
    let themes = await this._getThemes()

    for(let theme of themes)
      this.renderCollection.append(theme.xpath, theme)

    return this.renderCollection.collection
  }
}

window.vitrineApp.clients.get(token).publicLibraries.render = new RenderLibrary()